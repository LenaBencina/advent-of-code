__author__ = 'Lena'

import re

# this is the original input
input1 = {'children': 3, 'cats': 7, 'samoyeds': 2, 'pomeranians': 3, 'akitas': 0,
         'vizslas': 0, 'goldfish': 5, 'trees': 3, 'cars': 2, 'perfumes': 1}

# this is reduced input
input2 = {'children': 3, 'samoyeds': 2, 'akitas': 0, 'vizslas': 0, 'cars': 2, 'perfumes': 1}

# this are the atributes that must be greater or lesser
input_change = {'trees': '+', 'cats': '+', 'goldfish': '-', 'pomeranians': '-'}

with open('Advent16.txt', 'r') as myfile:
    data = myfile.readlines()

def find_sue(input1, input2, input_change, data, part):
    for sue in data:
        sue = sue.strip() # strip line
        info, sue = sue.split(': ', 1)[1],  sue.split(': ', 1)[0]  # get attributes and number of sue
        keys, values = re.findall(r'[a-z]+', info), re.findall(r'\d+', info)  # parse words and numbers
        info_dict = {key:int(value) for key, value in zip(keys, values)}  # put them into dict
        match = set(info_dict.items()).issubset(input1.items())  # check if match the input
        if part == 1 and match: return sue

        # second part
        match_changed = True
        new_dict = {key:value for key,value in info_dict.items() if key not in input_change}  # make dict without changed values
        match = set(new_dict.items()).issubset(input2.items())  # check if new (reduced) dict match the input
        if part == 2 and match:  # now check changed values
            for key, value in input_change.items():  # iterate through dict with changed values
                if key in info_dict:
                    if value == '+' and info_dict[key] > input1[key] or value == '-' and info_dict[key] < input1[key]:
                        continue  # if match the criteria check the next one
                    else:  # if not match the criteria go to next Sue
                        match_changed = False
                        break

            if match_changed:
                return sue

print(find_sue(input1, input2, input_change, data, 1))
print(find_sue(input1, input2, input_change, data, 2))