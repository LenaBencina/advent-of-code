__author__ = 'Lena'

import string
import re

password = 'hepxcrrq'

alphabet = string.ascii_lowercase
consecutive = [(alphabet[i]+alphabet[i+1]+alphabet[i+2]) for i in range(len(alphabet)-2)] # list: abc, bcd, cde,...
regex = re.compile(r'^[^i|o|l]+$')  # regex for finding i or o or l to filter list of consecutives
filtered = list(filter(lambda x: regex.search(x), consecutive))  # remove elements containing i,o or l

ok = False  # when ok is True, new password is found
password_list = list(reversed(password))  # convert password to list
passwords = 0 # for counting passwords (second part)

while not ok:
    for i in range(0,len(password_list)):
        if password_list[i] == 'z':
            new_letter = 'a'
            password_list[i] = new_letter  # change letter
            continue

        else:
            new_letter = chr(ord(password_list[i]) + 1)  # 'increase' letter
            if new_letter in 'iol':
                new_letter = chr(ord(new_letter) + 1)

            password_list[i] = new_letter  # change letter

        password = ''.join(reversed(password_list))
        match = re.findall(r'(\w)\1',password) # match string with two same ch. in a row; aa, bb, cc, ...

        if len(set(match)) > 1 and any(x in password for x in filtered):  # if match all the conditions, we found our new password
            passwords += 1
            if passwords == 2: # second part
                ok = True
            break

        break  # start over

print(password)