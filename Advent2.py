__author__ = 'Lena'

with open('Advent2.txt', 'r') as myfile:
    data=myfile.readlines()
    TotalPaperArea = 0
    TotalRibbon = 0
    for present in data:
        present = present.strip().split('x')
        present = [ int(x) for x in present ]
        present.sort()
        area = 2*present[0]*present[1] + 2*present[0]*present[2] + 2*present[1]*present[2] + present[0]*present[1]
        ribbon = 2*present[0] + 2*present[1] + present[0]*present[1]*present[2]
        TotalPaperArea = TotalPaperArea + area
        TotalRibbon = TotalRibbon + ribbon

    print(TotalPaperArea)
    print(TotalRibbon)