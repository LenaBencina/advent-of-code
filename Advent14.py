__author__ = 'Lena'

import re

with open('Advent14.txt', 'r') as myfile:
    data = myfile.readlines()

    info = {}
    for line in data:
        numbers = re.findall(r'\d+',line)
        name = re.search(r'^\w+', line).group()  # get name
        numbers = list(map(int, numbers))  # convert to int
        info[name] = numbers

    names = list(info.keys())
    distances = dict((name,0) for name in names)  # create dict for distances

    points = dict((name,0) for name in names)  # second part - create dict for points

    time = 1
    while time <= 2503:
        for key, value in info.items(): # iterate over dict
            running_time, resting_time = value[1], value[2]
            one_round = running_time + resting_time
            if (time % one_round) > 0 and (time % one_round) <= running_time: # if moving, add distance
                distances[key] += value[0]

        ####### second part #######
        currently_best = max(list(distances.values()))  # get the leading distance
        names_of_best = [name for name, distance in distances.items() if distance == currently_best]  # get names
        for name in names_of_best:
            points[name] += 1  # add one point
        ############################################################################################

        time += 1

    best_first_part = max(list(distances.values()))
    best_second_part = max(list(points.values()))
    print(best_first_part)
    print(best_second_part)