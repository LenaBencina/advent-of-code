__author__ = 'Lena'

import re

with open('Advent12.txt', 'r') as myfile:
        data = myfile.readlines()
        data_string = ''.join(data)  # convert to string
        numbers = re.findall(r'-?\d+',data_string)  # find all numbers
        numbers_int = list(map(int, numbers))  # convert to integers
        sum = sum(numbers_int)  # get sum
        print(sum)