__author__ = 'Lena'

import re

input = 3113322113
input_string = str(input)

for i in range(40): #for second part change 40 to 50
    list_of_repetitions = [m.group(0) for m in re.finditer(r"(\d)\1*", input_string)] #get list of repetitions
    new_input = ''
    for digits in list_of_repetitions:
        new_input += str(len(digits)) + str(digits[0])
    input_string = new_input

print(len(new_input))