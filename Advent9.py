__author__ = 'Lena'

import re
import itertools

with open('Advent9.txt', 'r') as myfile:
        data = myfile.readlines()
        dict = {}

        for line in data:
            line = line.strip()
            cities = re.findall(r'\b[A-Z].*?\b',line) #get cities
            distance = int(re.search(r'\d+',line).group()) #get distance
            dict.setdefault(cities[0],{})[cities[1]]=distance
            dict.setdefault(cities[1],{})[cities[0]]=distance

        cities = list(dict.keys())
        routes = list(itertools.permutations(cities))
        distances = []

        for route in routes:
            distance = 0
            for i in range(0,len(route)-1):
                city1,city2 = route[i],route[i+1]
                distance += dict[city1][city2]
            distances.append(distance)

        shortest = min(distances)
        longest = max(distances)

print(shortest,longest)