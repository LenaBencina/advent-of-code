__author__ = 'Lena'

import re

with open('Advent5.txt', 'r') as myfile:
    data=myfile.readlines()
    counter = 0
    for word in data:
        #checks if word contains ab, cd, pq, or xy
        if "ab" in word or "cd" in word or "pq" in word or "xy" in word:
            continue
        #checks if word contains at least three vowels
        match1 = re.match(r'((.*[aeiou]){3})', word)
        if match1:
            #checks if word contains at least one letter that appears twice in a row
            match2 = re.match(r'(\w*)(\w)\2(\w*)', word)
            if match2:
                counter +=1

print(counter)


