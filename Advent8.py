__author__ = 'Lena'

import re

with open('Advent8.txt', 'r') as myfile:
        data = myfile.readlines()
        total_code = 0
        total_memory = 0

        for line in data:
            line = line.strip()
            total_code += len(line)

            line_stripped = line[1:-1]
            match1 = re.findall(r'\\"',line_stripped) #finds all \"
            length1 = len(match1)

            match2 = re.findall(r'\\x\w\w',line_stripped) #finds all \x + two ch.
            length2 = len(match2)

            match3 = re.findall(r'\\\\',line_stripped) #finds all \\
            length3 = len(match3)

            memory = len(line) - length1 - length2*3 - length3 - 2

            match4 = re.findall(r'\w\\\\x|^\\\\x|"\\\\x',line_stripped) #finds all \\x + two ch., but not \\\x + two ch.

            if len(match4) > 0:
                memory += 3

            total_memory += memory

        result = total_code - total_memory
        print(result)



#easy way

total_code = 0
total_memory = 0

for line in data:
    line = line.strip()
    total_code += len(line)
    total_memory += len(eval(line))

result = total_code - total_memory
print(result)
