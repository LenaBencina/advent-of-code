__author__ = 'Lena'

import re
import itertools

number_of_people = 8
with open('Advent13.txt', 'r') as myfile:
    data = myfile.readlines()

def get_info(number_of_people, data):
    happiness_info, neighbours = {}, {}
    counter = 0
    for line in data:
        line = line.strip()
        names = re.findall(r'\b[A-Z][a-z]+\b', line)  # get names
        person, neighbour = names[0],names[1]
        number = int(re.search(r'\d+', line).group())  # get amount of happiness
        if 'lose' in line:
            number = -number  # if she/he loses it is a negative number
        neighbours[neighbour] = number

        counter += 1
        if counter == number_of_people - 1:
            counter = 0
            neighbours['me'] = 0 ### second part
            happiness_info[person] = neighbours  # add dict of all neighbours
            neighbours = {}
    return happiness_info

happiness_info = get_info(number_of_people, data)




def get_optimal_seating_arrangement(happiness_info, number_of_people):

    names = list(happiness_info.keys())

    ### second part ###
    number_of_people += 1
    my_dict = dict((name,0) for name in names)
    happiness_info['me'] = my_dict
    names.append('me')
    ############################################

    permutations = list(itertools.permutations(names))  # get all possible seating arrangements
    all_changes = []
    total_change = 0

    for seating_arrangement in permutations:
        for position in range(0, number_of_people):
            person = seating_arrangement[position]
            if position == 0:
                first_neighbour, second_neighbour = seating_arrangement[position+1], seating_arrangement[number_of_people-1]
            elif position == number_of_people - 1:
                first_neighbour, second_neighbour = seating_arrangement[0], seating_arrangement[position-1]
            else:
                first_neighbour, second_neighbour = seating_arrangement[position+1], seating_arrangement[position-1]

            total_change += happiness_info[person][first_neighbour] + happiness_info[person][second_neighbour] # sum the amount of happiness

        all_changes.append(total_change)
        total_change = 0

    return max(all_changes)

result = get_optimal_seating_arrangement(happiness_info, number_of_people)
print(result)