__author__ = 'Lena'

import re

lights = [[False for x in range(1000)] for x in range(1000)]
lightsPower = [[0 for x in range(1000)] for x in range(1000)]

with open('Advent6.txt', 'r') as myfile:
    data=myfile.readlines()
    for instruction in data:
        coordinates = re.findall('\d+', instruction) #get digits out of instructions
        start_x, start_y = int(coordinates[0]),int(coordinates[1])
        finish_x, finish_y = int(coordinates[2]),int(coordinates[3])
        for x in range(start_x,finish_x+1):
            for y in range(start_y,finish_y+1):
                if "turn on" in instruction:
                    lights[x][y] = True
                    lightsPower[x][y] += 1
                elif "turn off" in instruction:
                    lights[x][y] = False
                    lightsPower[x][y] = max(lightsPower[x][y] - 1,0)
                elif "toggle" in instruction:
                    lights[x][y] = not lights[x][y] #switch
                    lightsPower[x][y] += 2


    numberOfLights,powerOfLights  = 0,0

    for line1,line2 in zip(lights,lightsPower): #count
        numberOfLights += sum(line1)
        powerOfLights += sum(line2)


    print(numberOfLights)
    print(powerOfLights)
