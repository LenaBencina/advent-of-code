__author__ = 'Lena'

import re

operators = {'AND': lambda s: s[0] & s[1],
            'OR': lambda s: s[0] | s[1],
            'RSHIFT': lambda s: s[0] >> s[1],
            'LSHIFT': lambda s: s[0] << s[1],
            'NOT': lambda s: ~s[0]}

dict = {}

def find_path_to_a(wire,operators,dict):
    if wire in dict: #if already calculated
            return(dict[wire], dict)

    with open('Advent7.txt', 'r') as myfile:
        data = myfile.readlines()

        for instruction in data:
            instruction = instruction.strip()
            regex = r'.* ' + str(wire) + '$'
            match = re.match(regex, instruction) #match instruction to get which wire is connected to the next wire
            if match:
                cifra = re.search(r'^\d+(?=\s->\s\w+)',instruction) #get number from starting instruction (example: 1243 -> b)
                if cifra != None:
                    cifra = int(cifra.group())
                    dict[wire] = cifra

                    #********only for second part************
                    if wire == 'b':
                        dict[wire] = 46065
                        cifra = 46065
                    #****************************************

                    return cifra,dict

                path = instruction

        wires = re.findall('([a-z]+)', path) #get all wires out of instructions
        left_side,right_side = wires[:len(wires)-1],wires[len(wires)-1] #split
        for w in left_side:
            dict[w], dict = find_path_to_a(w,operators,dict) #do the same for all wires on the left side

        operator = re.search(r'AND|OR|RSHIFT|LSHIFT|NOT',path) #get operator if exist

        if operator != None:
            operator = operator.group()
            list_of_known_values = [dict[x] for x in left_side] #get known values from dict

            digit = re.search(r'\d+',path)
            if digit != None: #get digits for operator SHIFT or AND

                list_of_known_values.append(int(digit.group()))

            dict[right_side] = operators[operator](list_of_known_values)

        else: #without operator
            dict[right_side] = dict[left_side[0]]


    return(dict[right_side], dict)

result = find_path_to_a('a',operators,dict)
print(result)

