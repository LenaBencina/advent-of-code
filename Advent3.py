__author__ = 'Lena'

#first part: Santa alone
def SantaAlone():
    with open('Advent3.txt', 'r') as myfile:
        data=myfile.read()
        poisitions = {(0,0)} #set of positions
        tempPosition = [0,0] #temporary position
        for direction in data:
            tempPosition = GetPosition(direction,tempPosition)
            poisitions.add(tuple(tempPosition)) #add position
    return(len(poisitions))

#second part: Santa & Robo Santa
def SantaWithRobo():
    with open('Advent3.txt', 'r') as myfile:
        data=myfile.read()

        poisitionsSanta, poisitionsRoboSanta = {(0,0)}, {(0,0)} #sets of positions of visited houses
        tempPositionSanta, tempPositionRoboSanta = [0,0], [0,0] #starting positions

        for i,j in zip(range(0,len(data),2),range(1,len(data),2)):
            directionSanta, directionRoboSanta = data[i],data[j] #get directions for both
            tempPositionSanta = GetPosition(directionSanta, tempPositionSanta) #get position (list) for Santa
            tempPositionRoboSanta = GetPosition(directionRoboSanta, tempPositionRoboSanta) #get position (list) for Robo Santa
            poisitionsSanta.add(tuple(tempPositionSanta)) #add position for Santa
            poisitionsRoboSanta.add(tuple(tempPositionRoboSanta)) #add position for RoboSanta

        return(len(poisitionsSanta.union(poisitionsRoboSanta))) #return the number of all houses they visited, which is the union of all the positions

#auxiliary function for getting temporary position
def GetPosition(direction,position):
    if direction == ">":
        position[0] = position[0] + 1
    elif direction == "<":
        position[0] = position[0] - 1
    elif direction == "^":
        position[1] = position[1] + 1
    elif direction == "v":
        position[1] = position[1] - 1
    return(position)

#print(SantaAlone())
#print(SantaWithRobo())